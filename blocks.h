// Modify this file to change what commands output to your statusbar, and
// recompile using the make command.
static const Block blocks[] = {
    /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/

    {" Vol:",
     "pamixer --get-volume-human",
     0, 10},

    // It stresses me out to look at my cpu
    // {"Cpu:", "/home/mel/Scripts/dwmblocks/cpu.sh", 5, 0},

    {"Bat:", "acpi | awk '{print $4}' | sed s/,//g", 60, 0},

    {"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g", 5, 0},

    {"", "date '+%b %d (%a) %I:%M%p'", 5, 0},
};

// sets delimeter between status commands. NULL character ('\0') means no
// delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
